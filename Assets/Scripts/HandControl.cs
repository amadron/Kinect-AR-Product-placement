﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandControl : MonoBehaviour {
    bool manipulate;
    public float distance;
    Vector3 lastPos;
    public GameObject thumb;
    GameObject shirt;
    Material color;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (thumb != null)
        {
            distance = Vector3.Distance(transform.position, thumb.transform.position);
            if (distance < 0.31)
                Debug.Log("Grab");
        }

	}

    private void OnTriggerEnter(Collider other)
    {
        if (name.Contains("Left") && other.name.Contains("Left"))
        {
            Debug.Log("Enter");
            manipulate = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (name.Contains("Left") && other.name.Contains("Left"))
        {
            Debug.Log("Exit");
            manipulate = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        

    }
}
