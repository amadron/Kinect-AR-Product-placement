﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureLogic : MonoBehaviour {
    public GameObject HandLeft;
    public GameObject HandRight;
    public GameObject TipLeft;
    public GameObject TipRight;
    public float handDist;
    public float range = 10;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        handDist = Vector3.Distance(HandLeft.transform.position, HandRight.transform.position);
        Vector3 rotL = HandLeft.transform.parent.localRotation.eulerAngles;
        Vector3 rotR = HandRight.transform.parent.localRotation.eulerAngles;
        bool lRight = false;
        bool rRight = false;
        if (rotL.z > 90 - range && rotL.z < 90 + range)
            lRight = true;
        if (rotR.z > 90 - range && rotR.z < 90 + range)
            lRight = true;
        if (lRight && rRight)
            Debug.Log("Gesture Ready");
    }
}
