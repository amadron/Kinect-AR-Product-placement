﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace JointOrientationBasics
{
    public class HandSwitch : MonoBehaviour
    {
        public MegaPointCache pla;

        // Use this for initialization
        void Awake()
        {
            pla = transform.parent.gameObject.GetComponentInChildren<MegaPointCache>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            if(pla == null)
            foreach(Transform tr in transform.parent.parent)
            {
                if (tr.name.Contains("SpineMid") && tr.childCount > 1)
                {
                    pla = tr.GetChild(1).GetComponent<MegaPointCache>();
                    Debug.Log("Found");
                }

            }
            if (pla != null)
            {
                pla.speed *= -1;
                pla.animated = true;
                if (pla.speed < 0)
                    pla.time = 3;
                else
                    pla.time = 0;
            }
            else
            {
                Debug.Log("Empty");
            }
        }
    }
}